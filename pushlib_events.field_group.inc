<?php
/**
 * @file
 * pushlib_events.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function pushlib_events_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_events|node|post|form';
  $field_group->group_name = 'group_events';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'post';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Pushlib Events',
    'weight' => '8',
    'children' => array(
      0 => 'field_event_date',
      1 => 'field_event_location',
      2 => 'field_adress',
    ),
    'format_type' => 'tabs',
    'format_settings' => array(
      'formatter' => '',
      'instance_settings' => array(
        'classes' => '',
      ),
    ),
  );
  $export['group_events|node|post|form'] = $field_group;

  return $export;
}
