<?php
/**
 * @file
 * pushlib_events.features.uuid_term.inc
 */

/**
 * Implements hook_uuid_features_default_terms().
 */
function pushlib_events_uuid_features_default_terms() {
  $terms = array();

  $terms[] = array(
    'name' => 'Workshop',
    'description' => '<p>gemeinsam mit meinem Kollegen helfe ich jungen Wissenschaftlern und -innen, komplexe Sachen verständlich zu formulieren.</p>',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => '0ff18cb5-0325-4b56-9ea4-50d6feea5aff',
    'vocabulary_machine_name' => 'news',
    'field_service_typ' => array(
      'und' => array(
        0 => array(
          'value' => 'Teaching',
        ),
      ),
    ),
    'field_news_term_image' => array(),
    'parent' => array(
      0 => '2f9c6bdc-915a-41f4-a44b-6a43c6fb0d7d',
    ),
  );
  $terms[] = array(
    'name' => 'Lesung',
    'description' => '<p><br>Ich Lese Aus meinen <a href="taxonomy/term/4">Büchern</a></p>',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => '2a025c61-0746-4d97-aed9-08dc2e868fbb',
    'vocabulary_machine_name' => 'news',
    'field_service_typ' => array(
      'und' => array(
        0 => array(
          'value' => 'Speaking',
        ),
      ),
    ),
    'field_news_term_image' => array(),
    'parent' => array(
      0 => '2f9c6bdc-915a-41f4-a44b-6a43c6fb0d7d',
    ),
  );
  $terms[] = array(
    'name' => 'Event',
    'description' => '<br>',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => '2f9c6bdc-915a-41f4-a44b-6a43c6fb0d7d',
    'vocabulary_machine_name' => 'news',
    'field_service_typ' => array(),
    'field_news_term_image' => array(),
  );
  $terms[] = array(
    'name' => 'Vortrag',
    'description' => '<br>',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => '7b4cc6ba-f300-4ca9-9819-8f07993082b2',
    'vocabulary_machine_name' => 'news',
    'field_service_typ' => array(),
    'field_news_term_image' => array(),
    'parent' => array(
      0 => '2f9c6bdc-915a-41f4-a44b-6a43c6fb0d7d',
    ),
  );
  $terms[] = array(
    'name' => 'Funk, Fernsehen, Zeitschriften',
    'description' => '<p>Ich mache selbst sendungen oder bin als gast zugegen</p>',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => 'a68af81a-0991-44c4-8e78-9605ee445f6c',
    'vocabulary_machine_name' => 'news',
    'field_service_typ' => array(),
    'field_news_term_image' => array(),
    'parent' => array(
      0 => '2f9c6bdc-915a-41f4-a44b-6a43c6fb0d7d',
    ),
  );
  return $terms;
}
